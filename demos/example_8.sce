// =============================================================================
// Test problem 8 (Rosen-Suzuki)
// =============================================================================
function example_8()
nb_constr_test_8 = 3;
xopt_test_8 = [0 1 2 -1]';

function [f, con, info] = test_8(x)
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  d__4 = x(4);
  f = d__1 * d__1 + d__2 * d__2 + d__3 * d__3 * 2 + d__4 * d__4 - x(1) * 5. - x(2) * 5. - x(3) * 21. + x(4) * 7.;
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  d__4 = x(4);
  con(1) = 8. - d__1 * d__1 - d__2 * d__2 - d__3 * d__3 - d__4 * d__4 - x(1) + x(2) - x(3) + x(4);
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  d__4 = x(4);
  con(2) = 10. - d__1 * d__1 - d__2 * d__2 * 2. - d__3 * d__3 - d__4 * d__4 * 2. + x(1) + x(4);
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  con(3) = 5. - d__1 * d__1 * 2. - d__2 * d__2 - d__3 * d__3 - x(1) * 2. + x(2) + x(4);
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_8);
[x_opt, status, eval_func] = cobyla(x0, test_8, nb_constr_test_8, rhobeg, rhoend, message_in, eval_func_max);

printf("This problem is taken from page 66 of Hock and Schittkowski''s book Test\n");
printf("Examples for Nonlinear Programming Codes. It is their test problem Number\n");
printf("43, and has the name Rosen-Suzuki.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_8));
endfunction
// =============================================================================
example_8();
clear example_8;
// =============================================================================
