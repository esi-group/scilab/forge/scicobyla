// ====================================================================
// Yann COLLETTE - DIGITEO 2010
// Allan CORNET - DIGITEO - 2011
// ====================================================================
function build_src()
  src_dir = get_absolute_file_path("builder_src.sce");
  tbx_builder_src_lang("c", src_dir);
endfunction
// ====================================================================
build_src();
clear build_src;
// ====================================================================