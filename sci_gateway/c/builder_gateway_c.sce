// ====================================================================
// Allan CORNET - DIGITEO - 2011
// Yann COLLETTE - DIGITEO - 2010
// ====================================================================
function build_gw()

if getos() == 'Windows' then
  // to manage long pathname
  includes_src_c = '-I""' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c""';
else
  includes_src_c = '-I' + get_absolute_file_path('builder_gateway_c.sce') + '../../src/c';
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %f;

TABLE = ['cobyla','sci_cobyla'];
FILES = ['sci_cobyla.c'];

tbx_build_gateway('cobyla_c', TABLE, FILES, get_absolute_file_path('builder_gateway_c.sce'), ['../../src/c/libcobyla'],'',includes_src_c);
clear WITHOUT_AUTO_PUTLHSVAR;

endfunction
// ====================================================================
build_gw();
clear build_gw;
// ====================================================================

