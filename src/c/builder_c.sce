// ====================================================================
// Yann COLLETTE - DIGITEO - 2010
// Allan CORNET - DIGITEO - 2011
// ====================================================================
function builder_c()
  src_c_path = get_absolute_file_path('builder_c.sce');
  CFLAGS = "-I" + src_c_path;
  tbx_build_src(['cobyla'], ['cobyla.c'], 'c', src_c_path, '', '', CFLAGS);
endfunction
// ====================================================================
builder_c();
clear builder_c
// ====================================================================
