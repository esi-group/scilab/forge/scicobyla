// =============================================================================
// Test problem 2 (2D unit circle calculation)
// =============================================================================
function example_2()

nb_constr_test_2 = 1;
xopt_test_2 = sqrt(5) * [1 -1]';

function [f, con, info] = test_2(x)
  f = x(1) * x(2);
  d__1 = x(1);
  d__2 = x(2);
  con(1) = 1. - d__1 * d__1 - d__2 * d__2;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_2);
[x_opt, status, eval_func] = cobyla(x0, test_2, nb_constr_test_2, rhobeg, rhoend, message_in, eval_func_max);

printf("Easy two dimensional minimization in unit circle.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_2));
endfunction
// =============================================================================
example_2();
clear example_2;
// =============================================================================