// Test problem 7 (Equation (14.4.2) in Fletcher's book)
// =============================================================================
function example_7()
nb_constr_test_7 = 3;
xopt_test_7 = [0 -3 -3]';

function [f, con, info] = test_7(x)
  f = x(3);
  con(1) = x(1) * 5. - x(2) + x(3);
  d__1 = x(1);
  d__2 = x(2);
  con(2) = x(3) - d__1 * d__1 - d__2 * d__2 - x(2) * 4.;
  con(3) = x(3) - x(1) * 5. - x(2);
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_7);
[x_opt, status, eval_func] = cobyla(x0, test_7, nb_constr_test_7, rhobeg, rhoend, message_in, eval_func_max);

printf("This problem is taken from Fletcher''s book Practical Methods of\n");
printf("Optimization and has the equation number (14.4.2).\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_7));
endfunction
// =============================================================================
example_7();
clear example_7;
// =============================================================================
