// =============================================================================
// Test problem 6 (Equation (9.1.15) in Fletcher's book)
// =============================================================================
function example_6()

nb_constr_test_6 = 2;
xopt_test_6 = sqrt(5) * [1 1]';

function [f, con, info] = test_6(x)
  f = -x(1) - x(2);
  d__1 = x(1);
  con(1) = x(2) - d__1 * d__1;
  d__1 = x(1);
  d__2 = x(2);
  con(2) = 1. - d__1 * d__1 - d__2 * d__2;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_6);
[x_opt, status, eval_func] = cobyla(x0, test_6, nb_constr_test_6, rhobeg, rhoend, message_in, eval_func_max);

printf("This problem is taken from Fletcher''s book Practical Methods of\n");
printf("Optimization and has the equation number (9.1.15).\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_6));
endfunction
// =============================================================================
example_6();
clear example_6;
// =============================================================================