// =============================================================================
// Test problem 3 (3D ellipsoid calculation)
// =============================================================================
function example_3()

nb_constr_test_3 = 1;
xopt_test_3 = [1/sqrt(3) 1/sqrt(6) -1/3]';

function [f, con, info] = test_3(x)
  f = x(1) * x(2) * x(3);
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  con(1) = 1. - d__1 * d__1 - d__2 * d__2 * 2. - d__3 * d__3 * 3.;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_3);
[x_opt, status, eval_func] = cobyla(x0, test_3, nb_constr_test_3, rhobeg, rhoend, message_in, eval_func_max);

printf("Easy three dimensional minimization in ellipsoid.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_3));
endfunction
// =============================================================================
example_3();
clear example_3;
// =============================================================================
