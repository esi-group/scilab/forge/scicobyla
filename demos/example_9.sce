// =============================================================================
// Test problem 9 (Hock and Schittkowski 100)
// =============================================================================
function example_9()

nb_constr_test_9 = 4;
xopt_test_9 = [2.330499 1.951372 -0.4775414 4.365726 -0.624487 1.038131 1.594227]';

function [f, con, info] = test_9(x)
  d__1 = x(1) - 10.;
  d__2 = x(2) - 12.;
  d__3 = x(3)^2;
  d__4 = x(4) - 11.;
  d__5 = x(5)^2;
  d__6 = x(6);
  d__7 = x(7)^2;
  f = d__1 * d__1 + d__2 * d__2 * 5. + d__3 * d__3 + d__4 * d__4 * 3. ...
      + d__5 * (d__5 * d__5) * 10. + d__6 * d__6 * 7. + d__7 * d__7 ...
      - x(6) * 4. * x(7) - x(6) * 10. - x(7) * 8.;
  d__1 = x(1);
  d__2 = x(2)^2;
  d__3 = x(4);
  con(1) = 127. - d__1 * d__1 * 2. - d__2 * d__2 * 3. - x(3) - d__3 * d__3 * 4. - x(5) * 5.;
  d__1 = x(3);
  con(2) = 282. - x(1) * 7. - x(2) * 3. - d__1 * d__1 * 10. - x(4) + x(5);
  d__1 = x(2);
  d__2 = x(6);
  con(3) = 196. - x(1) * 23. - d__1 * d__1 - d__2 * d__2 * 6. + x(7) * 8.;
  d__1 = x(1);
  d__2 = x(2);
  d__3 = x(3);
  con(4) = d__1 * d__1 * -4. - d__2 * d__2 + x(1) * 3. * x(2) - d__3 * d__3 * 2. - x(6) * 5. + x(7) * 11.;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_9);
[x_opt, status, eval_func] = cobyla(x0, test_9, nb_constr_test_9, rhobeg, rhoend, message_in, eval_func_max);

printf("This problem is taken from page 111 of Hock and Schittkowski''s\n");
printf("book Test Examples for Nonlinear Programming Codes. It is their\n");
printf("test problem Number 100.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_9));
endfunction
// =============================================================================
example_9();
clear example_9;
// =============================================================================
