// =============================================================================
// Test problem 5 (Intermediate Rosenbrock)
// =============================================================================
function example_5()

nb_constr_test_5 = 0;
xopt_test_5 = [-1 1]';

function [f, con, info] = test_5(x)
  d__2 = x(1);
  d__1 = d__2 * d__2 - x(2);
  d__3 = x(1) + 1.;
  f = d__1 * d__1 * 10. + d__3 * d__3;
  con = 0;
  info = 0;
endfunction

//
// Start COBYLA optimization
//

rhobeg        = 1;
rhoend        = 1e-3;
message_in    = 3;
eval_func_max = 200;

x0 = ones(xopt_test_5);
[x_opt, status, eval_func] = cobyla(x0, test_5, nb_constr_test_5, rhobeg, rhoend, message_in, eval_func_max);

printf("Intermediate version of Rosenbrock''s problem.\n");
printf("Iterations needed to solve the problem: %d - status = %d\n", eval_func, status);
printf("solution found:"); disp(x_opt);
printf("comparison to the best know solution: %f\n", norm(x_opt - xopt_test_5));
endfunction
// =============================================================================
example_5()
clear example_5;
// =============================================================================
