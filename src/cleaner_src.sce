// ====================================================================
// CORNET Allan - DIGITEO - 2011
// ====================================================================
function cleaner_src()
  src_dir = get_absolute_file_path("cleaner_src.sce");

  for language = ["c"];
    cleaner_file = src_dir + filesep() + language + filesep() + "cleaner.sce";
    if isfile(cleaner_file) then
      exec(cleaner_file);
      mdelete(cleaner_file);
    end
  end
endfunction
// ====================================================================
cleaner_src();
clear cleaner_src; // remove cleaner_src on stack
// ====================================================================
